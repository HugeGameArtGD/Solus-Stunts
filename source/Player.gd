extends Node3D

func _ready():
	#get_tree().connect("screen_resized",Callable(self,"screen_resized"))
	screen_resized()

func screen_resized():
	$"Viewports/CenterViewport".size = get_viewport().size
	$"Viewports/LeftViewport".size = get_viewport().size/2
	$"Viewports/RightViewport".size = get_viewport().size/2

extends Node3D

func _ready():
	get_tree().connect("screen_resized",Callable(self,"screen_resized"))
	screen_resized()

func screen_resized():
	get_parent().get_node("center/CenterViewport").size = get_viewport().size
	get_parent().get_node("left/LeftViewport").size = get_viewport().size/2
	get_parent().get_node("right/RightViewport").size = get_viewport().size/2

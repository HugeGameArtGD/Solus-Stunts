extends Node
const DEFAULT_PORT = 10567
const MAX_PEERS = 12
var player_name = ""
var track = 0
var players = {}
var car_num = 1
var car_num2 = 1
var x = 0
var car_id = null

signal player_list_changed()
signal connection_failed()
signal connection_succeeded()
signal game_ended()
signal game_error(what)

func _player_disconnected(id):
	if (get_tree().is_network_server()):
		if (has_node("/root/world3D/world2")):
			emit_signal("game_error", "Player " + players[id] + " disconnected")
			end_game()
		else:
			unregister_player(id)
			for p_id in players:
				rpc_id(p_id, "unregister_player", id)

func _connected_ok():
	rpc("register_player", get_tree().get_instance_id(), player_name)
	emit_signal("connection_succeeded")

func _server_disconnected():
	emit_signal("game_error", "Server disconnected")
	end_game()

func _connected_fail():
	get_tree().set_network_peer(null)
	emit_signal("connection_failed")

@rpc func register_player(id, new_player_name):
	if (get_tree().is_network_server()):
		rpc_id(id, "register_player", 1, player_name) 
		for p_id in players:
			rpc_id(id, "register_player", p_id, players[p_id])
			rpc_id(p_id, "register_player", id, name)
	players[id] = new_player_name
	emit_signal("player_list_changed")

@rpc func unregister_player(id):
	players.erase(id)
	emit_signal("player_list_changed")

@rpc func pre_start_game(Track, car_number, _car_number2):
	$"/root/lobby/car".queue_free()
	var world = load("res://world.tscn").instantiate()
	$"/root/lobby".add_child(world)
	#$"/root/lobby/World".queue_free()
	$"/root/lobby".set_process_input(false)
	$"/root/lobby/World/Player".queue_free()
	$"/root/lobby/World/car_showcase".queue_free()
	
	var car_scene = load("res://car/" + str(car_number) + "/car" + str(car_number) + ".tscn")
	var car = car_scene.instantiate()
	car_id = get_tree().get_instance_id()
	car.set_name(str(car_id))
	#car.set_network_authority(get_tree().get_instance_id())
	car.set_player_name(player_name)
	car.visible_viewport()
	world.get_node("vehicles").add_child(car)
	world.car = car
	#$"/root/lobby/World/Player/CanvasLayer/Viewport".queue_free()
	
	#for pn in players:
	#	var car_scene2 = load("res://car/" + car_number2 + "/car" + car_number2 + ".tscn")
	#	car = car_scene2.instantiate()
	#	car.set_name(str(pn))
	#	car.set_network_master(pn)
	#	car.set_player_name(players[pn])
	#	world.get_node("vehicles").add_child(car)
		
	var newtrack = load("res://tracks/" + str(Track) + "/track" + str(Track) + ".tscn").instantiate()
	var viewport = $"/root/lobby/World/Player/CanvasLayer/Viewport/"
	match Track:
		1:
			set_background(load("res://hdri/umhlanga_sunrise_2k.hdr"))
			viewport.get_node("AudioStreamPlayer").stream = load("res://assets/music/Theme of Agrual.ogg")
		2:
			set_background(load("res://hdri/moonless_golf_2k.hdr"))
			viewport.get_node("AudioStreamPlayer").stream = load("res://assets/music/Tactical Pursuit.ogg")
		3:
			set_background(load("res://hdri/cape_hill_2k.hdr"))
			viewport.get_node("AudioStreamPlayer").stream = load("res://assets/music/Pleasant Creek.ogg")
		4:
			set_background(load("res://hdri/spruit_sunrise_2k.hdr"))
			viewport.get_node("AudioStreamPlayer").stream = load("res://assets/music/Pizza Dungeon.ogg")
		5:
			set_background(load("res://hdri/syferfontein_6d_clear_2k.hdr"))
			viewport.get_node("AudioStreamPlayer").stream = load("res://assets/music/Lively Meadow.ogg")
	viewport.get_node("AudioStreamPlayer").play()
	
	world.get_node("track").add_child(newtrack)
	post_start_game()
	#if (not is_network_authority()):
	#	# Tell server we are ready to start
	#	rpc_id(1, StringName("ready_to_start"), get_tree().get_instance_id())
	#elif players.size() == 0:
	#	post_start_game()

@rpc func post_start_game():
	get_tree().set_pause(false)

var players_ready = []

@rpc func ready_to_start(id):
	assert(get_tree().is_network_server())

	if (not id in players_ready):
		players_ready.append(id)

	if (players_ready.size() == players.size()):
		for p in players:
			rpc_id(p, StringName("post_start_game"))
		post_start_game()

func host_game(new_player_name):
	player_name = new_player_name
	var host = ENetMultiplayerPeer.new()
	host.create_server(DEFAULT_PORT, MAX_PEERS)
	host.set_target_peer(DEFAULT_PORT)

func join_game(ip, new_player_name):
	player_name = new_player_name
	var host = ENetMultiplayerPeer.new()
	host.create_client(ip, DEFAULT_PORT)
	host.set_target_peer(DEFAULT_PORT)

func get_player_list():
	return players.values()

func get_player_name():
	return player_name

func begin_game():
	#assert(is_network_authority())
	for p in players:
		rpc_id(p, StringName("pre_start_game"), track, car_num, car_num2)
	pre_start_game(track, car_num, car_num2)

func end_game():
	emit_signal("game_ended")
	players.clear()
	get_tree().set_network_peer(null)
	get_tree().change_scene("res://lobby.tscn")

func _ready():
	pass
	#var self_var = self
	#get_tree().connect("network_peer_connected", Callable(self_var, "_player_connected"))
	#get_tree().connect("network_peer_disconnected", Callable(self_var, "_player_disconnected"))
	#get_tree().connect("connected_to_server", Callable(self_var, "_connected_ok"))
	#get_tree().connect("connection_failed", Callable(self_var, "_connected_fail"))
	#get_tree().connect("server_disconnected", Callable(self_var, "_server_disconnected"))

func set_background(background):
	var env = $"/root/lobby/WorldEnvironment".environment
	var pan = PanoramaSkyMaterial.new()
	pan.panorama = background
	env.sky.sky_material = pan
